
## Signals
Signals are electric or electromagnetic impulses used to encode and transmit data, they can be analog or digital, computer networks and data/voice comms systems transmit signals
### Analog vs Digital
#### Analog
continuous wave form naturally occuring music and voice examples, it is harder to seperate noise from an analog signal than from digital <br>
noise is unwanted electrical or electromagnetic energy that degrades signal quality
![image-26.png](./image-26.png)
##### Digital
Discrete or non-continuous wave form, signal can only appear fixed number of times <br>
noise in digital signal: can still discern high voltage from low, if too much noise unable to discern
![image-27.png](./image-27.png)
### Fundamentals of Signals
#### Amplitude
Is the signal strength. Height of the wave above or below given reference point. Amp is measured in volts
![image-28.png](./image-28.png)
#### Frequency
Number of cycles per second <br>
 - number of times signal makes a complete cycle within a given time frame
    - measured in Hertz or cycles per second
- Spectrum
    - range of frequencies that a signal spans from min to max
- Bandwidth
    - absolute value of the difference between lowest and highest frequencies
![image-29.png](./image-29.png)
#### Phase
Position in time
    - position of the wave form relative to given moment of time or relative time to zero
    - change in phase can be any number of angles between 0-360 degrees
    - phase changes usually occur on common angles such as , 45-90-135 etc...
![image-30.png](./image-30.png)
## GPIO Pin references
- 40 pin General Purpose Input/Output (GPIO) header
    - gpio is interface for connecting components such as LED, resistors, breadboards, etc
    - Each of the 40 pins has a specific function most can be used as either input or output depending on program or its function
    - 3 ways to number pins
        - GPIO numbering: pin numbers assignes by Broadcom, processing chip manufacturer
        - Physical numbering: counting accross and down from pin 1
        - WiringPi GPIO numbering: pin number reference used by wiring Pi code
- Serial Peripheral Interface (SPI)
    - Synchronuous serial bus commonly used for sending data between micro controllers and small external peripherical devices
    - Multiple peripherical devices can be interface as SPI salves and be controlled with the SPI master
    - SPI uses separate clock (SCK) and data lines (MISO, MOSI) along with select line (SS) to choose among slave devices
    - RPI supports 2 chip select (CE) lines to interface with 2 SPI slaves devices : GPIO 7,8
- Inter-Intergrated Circuit (i2C)
    - i2c interface allows multiple slaves integrated circuits to communicate with one or more master
    - i2c uses two directional lines: serial data line (SDA) and serial clock line (SCL)
## JavaFX stage and scene
## DHCP discovery process
Dynamic host configuration protocol 
 - a standard method for internal devices to request and receive IP addresses and configuration information
DHCP server listens for broadcast Ethernet frame upon which it responds with an IP host address to be used at Layer 3 ; referred to as the DHCP discovery process. <br>
![image-39.png](./image-39.png)
## Domain name resolution
Domain name resolution is apart of address resolution. Address resolution is the process of finding an IP for a host name. <br>
DNS is a hierachical naming system that allows orgs to associate host names to IP addresses
    - servers store these asssociations and make the tables available for network users
    - servers keep up with the changing host names and make it easy to react to any org that changes its IP addresses
    - is crucial to making the internet a usable network
## IPv4 and IPv6
### Fields in IP header
![image-31.png](./image-31.png)
![image-32.png](./image-32.png)
### Valid IPv4 addresses
IPv4 uses classful network architecture. IP addresses are organized into five classes A B C D E <br>
A,B,C are used for networks. Each class restricted to a particular IP address range.
![image-33.png](./image-33.png)
![image-34.png](./image-34.png)
![image-35.png](./image-35.png)

### Valid IPv6 addresses
![image-36.png](./image-36.png)
### Size of IPv4 and IPv6 addresses
IPv4 = 2^32 addresses, uses 128-bit addresses, 32 bit addresses (4 bytes), four seperate 8-bit numbers (octets) octet value is between 0-255 <br>
IPv6 = 2^128 addresses, 8 groups of 4 hex numbers, first 64 bits is network last 64 is host (based on MAC), uses 128-bit addresses
### NAT and IPv4
IPv4 private networks contains private IP addresses, not routable to internet. The purpose of the private IP is to allow orgs to assign their own IP addresses from private network ranges. <br>
Network Address Translation (NAT) maps internal addresses to public routable addresses. NAT and private IP addresses make it possible to continue using IPv4 well into the future <br>
NAT lets router represent entire local area network to the internet as single IP addresses, which allows NAT to hide all the workstation IP addresses of a LAN from the internet.
## Simplex, half-duplex and full duplex communication nodes
### Simplex
THe transmission of the communication is unidirectional, sender and receiver occurs in one direction. The sender can only send data and the receiver can only receive it. Receiver cannot reply to sender. An example would be keyboard/monitor keyboard sends input to monitor and monitor displays it but monitor can't send data to keyboard.
### Half-duplex
Communication between sender and receiver occurs in both directions but only one at a time. The sender and receiver can both send and receive data but only one can send at any given time. An example would be walkie-talkies
### Full-duplex
The communication between sender and receiver can occur simultaneously. Both sender and receiver can transmit their data at the same time. An example would be a full duplex switch
## MQTT MQ Telemetry Transport
### General Information
MQTT is the most used messaging protocol for the Internet of Things (IoT). It is a set of rules that defines how IoT devices can publish and subscribe to data over the internet. <br>
It uses a publish and subscribe model, it is used for exchanging messages between IoT and industrial IoT devices.<br>
The sender (publisher) and the receiver (subscriber) communicat via Topics. Neither has awarness of the others existence and they communicate through a broker which is a 3rd party agent. <br>
The broke is the middleman between publish and subscribe. Its job is to filter all incoming messages and distribute it correctly to the Subscribers. The filtering makes it possible to control which client receives which message. <br>
The publish/subscribe pattern provides an alternative to traditional client-server architecture. <br>
MQTT uses TCP/IP to connect with the broker, most of the clients connect to the broker and remain connected even if they are not sending data. They publish a keepalive message at regular intervals which tells the broker that the client is connected. <br>
All clients are required to have a client name or ID, client names must also be unique, its used to track subscriptions, connecting with the same name as an existing client will drop the existing clients connection. <br>
A topic is created dynamicaly when either someone sub to the topic or someone publishes a message to the topic with retain message set to true. <br>
### Structure of MQTT Topics
MQTT topics are a form of addressing that allows MQTT cleints to share information. Topics are structured in a hierarchy similar to folders and files in a file system, using the / as a delimiter. <br>
It allows one to create a user friendly and self descriptive naming structure. A topic consists of one or more topic levels. <br>
Topic names satisft the following characteristics: <br>
- case sensitive
- Uses UTF-8 Strings
- must consits of at least one character
- there is no default or standard topic structure, except for $SYS topic
    - $SYS topic is a reserver topic used by MQTT broker to publish info about broker
- no topics are created on the broker except $SYS topic
### Wildcards
Wildcards can be used to subscribe to multiple topics simultaneously but it cannot be used to publish a message.
- Single level: +
    - replaces one topic level
    - myhome/groundfloor/+/temp
    - ![image-37.png](./image-37.png)
- Multi Level: #
    - covers many topic levels
    - hash symbol represents multi-level wild card in topic
    - multi-level wildcard must be placed at the last character in topic and preceded by /
    - myhome/groundfloor/#
    - ![image-38.png](./image-38.png)
### Process involved allowing IoT applications to exchange data over internet
The process is that MQTT allows clients to publish, subscribe and unsubscribe to topics that are published to a broker which is a 3rd party agent. This agent filters all of the messages to the appropriate client subscriptions. Each message must contain a topic that the broker can use to forward the message to interested clients. Message typically has a payload which contains the data to transmit in byte format
- use case of the client determines how payload is structured
- data can be binary, XML, text or JSON data
Payload is the actual content of the message. <br>
## TLS Handshake Protocol
The TLS protocol is designed to provide three essential services to all applications running above it
- encryption
- authentication
- data integrity <br>
Before the client and server can begin exchanging data over TLS the encrypted tunnel must be negotiated.
### What must communicating peers agree on in order to generate session key
- version of TLS protocol to use
- the cipher suite that will be used to provide confidentiality, integrity and authentication
- server certificate and optionally the client certificate must also be verified
generate session keys for encrypting messages between them after handshake is complete
### Why generate session keys?
Session keys offer an additional layer of protection because once the key has been used to establish a secure communication channel it is discarded. It is like a single use symmetric key. <br>
The first key exchanged in a message is a session key which allows two parties to continue data exchange without having to do any further key exchanges.
### TLS Use cases
A primary tls use case would be when encrypting the communication between web applications and web servers.
transmiting information such as passwords, credit card numbers and personal correspondance.
### cipher suite
A cipher suite is a set of algorithms that specifies details such as which shared encryption keys or session keys will be used for that particular session. The cipher suite provides a set of algorithms and protocols that are required to secure communications between clients and servers.
## Encoding Schemes
### ASCII and Extended ASCII
- ASCII
    - character encoding standard
    - uses 7 bit codes
    - 8th bit is unused (used as parity bit)
    - 2^7 = 128 codes
    - 2 general types of codes: 95 are "printable chars" (displayed on console) and 33 are "non-printable codes" (control features of console or comms channel)
    - mostly used in telecomms to control the transmission of data between terminal and host
    - limitations
        - missing the cent symbol
        - limitation on the number of characters represented
- Extended ASCII
    - encoding scheme to provide additional characters
    - sets 1 (ISO 8859-1) and 15 (ISO 8859-15) which represents european symbols
    - sets 2 used for eastern europe symbols
    - sets 10 used for nordic/inuit symbols
    - limitations
        - inability to reliably interchange document between computers using different code pages
        - code page : different chars using 8-bits , identical only in the first 128 codes (ASCII part)
        - better than ASCII but not enough for Asian languages
- Converting hex to binary 
- A -> 10
- B -> 11 
- C -> 12
- D -> 13
- E -> 14
- F -> 15 <br>
A9 -> Binary -> 1010 1001
### Unicode
character encoding system designed to support processing and display texts from diverse languages, each character is represented by a code point. <br>
Code point representation in memory is dependant on the encoding scheme (UTF-8, UTF-16) <br>
Total of 17 planes each with 2^16 code points = 1 114 112 code points, plane 0 is referred to as BMP, basic multilingual plane
- BMP representation
    - 4 hexadecimal digits U+XXXX(16 at bottom of X)
    - "Hello" -> U+0048 U+0065 U+006C U+006C U+006F
    - uses ascii or unicode table with hex, last two digits are the hex value of the chars
### UTF-16
all characters in modern languages are represented by 2-byte codes
- Big Endian -> most significant byte is first -> "Hello" -> 00 48 00 65 00 6C 00 6C 00 6F -> byte order mark FE FF
- Little Endian -> most significant byte is last -> "Hello" -> 48 00 65 00 6C 00 6C 00 6F 00 -> byte order mark is FF FE
### UTF-8
- 1 byte for standard english chars
- 2 bytes for latin and middle eastern chars
- 3 bytes for asian chars
- any additional chars can be represented using 4 bytes
UTF-8 is an optimal choice for HTMl because it is backwards compatible with ASCII since the first 128 chars are mapped to the same values
## Data Compression
Compression is a technique used to squeeze more data over a comms line or into a storage space
### lossy
when data is uncompressed, you do not have the complete original data, example: video game, movie, audio file, MPEG, JPEG, MP3
### lossless
when data is uncompressed, original data returns, example: financial file you want lossless, FLAC, run-length compression, huffman codes
## Noise
- white noise : thermal or gaussian noise, common source is thermal vibration of atoms in conductors
- impulse noise : most disruptive noise, random spikes of power can destroy one or more bits of info
- crosstalk : unwanted coupling between two different signal paths
- echo
- jitter : small timing irregularities during transmission of digital signals
- delay distortion
- attenuation
## Error detection and correction schemes
### simple parity check 
if performing even parity add a parity bit such that an even number of 1s are maintained, if odd parity check add parity bit to have odd number of 1s <br>
if 1001010 is sent using even parity 11001010 is sent back
- problem : simple parity only detects odd numbers of bits in error
### longitudinal parity
adds a parity bit to each character then adds a row of parity bits after block of characters, row of parity bits is a parity bit for each column of characters, row of parity plus column parity bits add a great amout of redundancy to a block of chars <br>
![image-40.png](./image-40.png) <br>
![image-41.png](./image-41.png)
### simple vs longitudinal
- both do not catch all errors
- simple only catches odd numbers of bit errors
- longitudinal is better at catching errors but requires too many check bits added to a block of data
### Arithmetic checksum
used in tcp and ip on the internet, characters to be transmitted are converted to numeric form and summed, sum is placed in some form at the end of transmission <br>
receiver perforsm same conversion and summing, compares new sum with sent sum <br>
![image-43.png](./image-43.png)
### Cyclic Redundancy Checksum
CRC error detection method treats the packet of data to be transmitted as a large polynomial. Transmitter takes the message polynomial and using polynomial arithmetic, divides it by a given generating polynomial. Quotient is discared but the remainder is attached to the end of the message. <br>
the message with remainder is transmitted, receiver divides the message and remainder by same generating polynomial, if remainder not equal to zero there was an error, if remainder = 0 then no error
### what protocol in tcp/ip stack uses which error detection scheme
Error detecion happens in the data link layer of the tcp/ip stack and a protocol that uses error detection scheme is IEEE 802.2
### why error detection schemes are not security schemes
Error detection schemes are meant to detect the error in data during transmission but they do not protect data they simple check if there was an error during transmission, error detection is meant to ensure data integrity <br>
Security schemes are meant to protect data against malicious threats during transmission. So no one can steal or alter the data.
## Wireless technologies
### cellular networks
to place a call you enter a phone number and press send, your cell will contact the nearest cell tower and grabs a set-up channel, your mobile id info is exchanged to make sure you are a current subscriber and then you are dynamically assigned two channels one for talking and the other for listening. <br>
receiving a call, your cell pings the nearest tower ever several seconds exchaning mobile id info when someone call you the cell phone system knows what phone to contact since you give it your information then the towar forwards the call to your phone.
### wireless signal frequency bands
Ranges of radio wave frequencies used to transmit data in the wireless spectrum. It can further be broken down itno WiFi channels.
- transmit voice, data, video over high frequency signals such as 2.4 GHz, 5GHz , 6 GHz
### IEEE designations: IEEE 802.11 & IEEE 802.16
802.11 is a standard that defines WLAN or WiFi, its optimized for nearly 100 meters. It does not provbide service throughout the coverage area to enable continuous connectivity. <br>
802.16 is a standard that defines WiMax products, optimized for 50km provides service throughout the coverage area to enable continuous connectivity. <br>
802.16 provide more scalability in usability while 802.11 provides less.
### relative transmission range wireless technologies
- terrestrial microwave transmission 20-30 miles
- satellite microwave transmission
    - LEO, low earth orbit 100-1000 miles out
    - MEO, Middle earth orbit 1000-22,300 miles out
    - GEO, geosynchronous earth orbit 22,300 miles
    - HEO, highly elliptical earth orbit follows, elliptical orbit
    - WiMax 20-30 miles
    - bluetooth 10cm to 10m can be extended to 100m by increasing power
    - Near field communications, very close distance or devices touching
### Wi-Fi LAN architecture
uses IEEE 802.11, wireless devices must have a client radio usually as a network card with an integrated antenna installed.
3 topologies for WLAN
- IBSS (Independant Basic Service Set)
- BSS (Basic Service Set)
- ESS (Extended Service Set)
#### IBSS
Similar to peer-to-peer networks, wireless devices configured to communicate withtout a wired network and none of the nodes serve as an access point
![image-45.png](./image-45.png)
#### BSS
BSS consists of at least one access point connected to wired network where all the other devices connect to the access point. <br>
So communication from Device A to Device B must first go through the access point so A -> AP -> B <br>
access point acts as a bridge between wired and wireless communications and perform basic routing functions
![image-46.png](./image-46.png)
#### ESS
ESS consists of two or more overlapping BSS' each containing an access point connected to wired network, mobile devices within ESS can seamlessly roam between APs
![image-47.png](./image-47.png)
## Cryptographic schemes
### schemes used to provide confidential services
Encryption, symmetric and asymmetric key encryption <br>
- Symmetric
    - uses same key for encrypt and decrypt
    - problem exchange the encryption key securely
    - faster
    - types of symmetric key algorithms
        - block ciphers, encrypt one block at a time
        - stream ciphers, encrypt one bit or byte at a time
- Asymmetric
    - 2 keys one public for encrypt and one private for decrypt
    - keys are manageable and no need to securely exchange public key
    - slower
    - asymmetric algorithms
        - RSA, most commonly used algorithm for asymmetric
### Schemes used to provide integrity services
- hash function
    - algorithm that takes data or message of any length and produces fix-length output called a message digest or fingerprint
    - to be useful must be strongly collision-free
        - collision occurs when 2 different message give different digest
    - MD5, RIPEMD-160, SHA-1, SHA-2, SHA-3 are examples of hash function
- Message authentication Code (MAC)
    - similar to message digest produced by hash functions but a secret key is required to produce MAC
    - HMAC (hash-based MAC) algorithm used along with secret key to produce MAC
        - any hash function can be used for HMAC
- password hashing
    - regular hashing is not enough for passwords
    - password hashing uses algorithms that slow down the speed of brute force attacks
- argon2
    - hash function that satisfied the hash-time parameter of password hashing
    - accepts 5 following parameters
        - salt length: 16 bytes rec
        - hash length: rec 16 or 32 bytes
        - iterations: affects time cost
        - memory: affect memory cost measured in kibibytes, 1k = 1024 bytes
        - parallelism: number of threads used by algorithm, affect degree of parallelism
    - 3 variants
        - Argon2d: resist GPU cracking attacks, good for cryptocurrency
        - Argon2i: resist side-channel attacks, good for password hashing
        - Argon2id: hybrid version
- Digital signature
    - uses pair of private-public keys and consists of signing algorithm and verification algorithm
    - signign algorithm uses private key to gen signature
    - verification algorithm uses signer's public key to decrypt signature
    - to reduce size of them the message is first hashed then message digest is signed
    - digital signature schemes
        - RSA
        - DSA
        - ECDSA
        - EdDSA
### recommended key size for RSA and AES crypto schemes
RSA key size : minimum 2048 bits
AES key size: 128, 192 or 256 bits 
### Elliptic curve crypto scheme that provides comparable security to RSA
ECDSA and EdDSA provide comparable security because they have a smaller key size compared to RSA but their security strength is just as equal
![image-44.png](./image-44.png)
### how does key size affect security
The longer the key is so the bigger the key size, the harder it is to crack. Key length is equal to the number of bits in an encryption algorithms key, a short key means poor security
### what are digital certificates
Digital certificates is an electronic document that binds the value of a public key, sender information, and the length of time the certificate is considered valid. It is issued by a certificate authority CA. The use cases of a digital certificate is for authentication a common use case would be the HTTPS protocol.
## WiFi Security
802.11i is a standard for WLANs that provides improved encryption for networks that use the popular 802.11 standards.
### characteristics of WEP, WPA, WPA2, WPA3
- WEP, Wired Equivalency Protocol, first security protocol used with WLANs, had weak 40-bit static RC4 keys and was too easy to break
- WPA, Wi-Fi Protected Access
    - major improvement including dynamic key encryption and mutual authentication 
    - uses RC4 encryption algorithm
- WPA2
    - uses AES instead of RC4
- WPA3
    - built upon WPA2 but provides higher security
## Authentication Schemes
Commonly used authentication schemes
- passwords
- Digital certificate
- radio-frequency identifaction (RFID) cards
- biometrics
- multi-factor
## Malware
software whose purpose is malicious, cause damage, steal info, steal resources, cause inconvenience
### types of malware
- virus: embeds itself to any other file once opened the virus will automatically execute its instructions and will do damage
- worm: standalone program that automatically spreads copies of itself to other computers
- trojan horse: program that is disguised as a harmless program
- ransomware: malware which can lock your computer and claim to reprsent the FBI and demand payment to get access to computer

# Coding questions

## simple javaFX such as hello world
public class HelloWorld extends Application {
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Hello World!");
        Button btn = new Button();
        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
 
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });
        
        StackPane root = new StackPane();
        root.getChildren().add(btn);
        primaryStage.setScene(new Scene(root, 300, 250));
        primaryStage.show();
    }
}

## code to interact with raspberry pi GPIO (senseLed.py)
public class ProcessBuilderEx {
    
    private String theOutput;
    
    public ProcessBuilder processBuilder;
    
    public ProcessBuilderEx(String theApp) {
        this.processBuilder = new ProcessBuilder();
   
        //Determine if the OS is MS Windows 
        boolean isWindows = System.getProperty("os.name")
                .toLowerCase().startsWith("windows");
        
        //List to store the command and the command arguments
        List<String> commandAndArgs;
        
        //Setup the command based on the OS type
        if (isWindows) {
            commandAndArgs = List.of("C:\\Dev\\python3", theApp);
            this.processBuilder.command(commandAndArgs);
        }
        else {
            commandAndArgs = List.of("/usr/bin/python3", theApp);
            this.processBuilder.command(commandAndArgs);
        }
    }
    
        //Start the process and get the output
    public String startProcess() throws IOException {
       
        //Initialize theOutput to null String
        this.theOutput = "";
        
        //Start the process
        var process = this.processBuilder.start();
        
        try (var reader = new BufferedReader(
            new InputStreamReader(process.getInputStream()))) {

            String line;

            while ((line = reader.readLine()) != null) {
                this.theOutput = this.theOutput + line;
            }

        }
        return this.theOutput;
    }
    
}

## code for controlling electronic components
### obtain data from sensor
 public void startSensorThread(){
        Thread sensorThread = new Thread(() -> {
            String theCmd = "src/main/Python/SenseLED.py";
            var theProcessBuilder = new ProcessBuilderEx(theCmd);
            try{
                String theOutput = theProcessBuilder.startProcess();                
                //takePictureAndVideoThread();
                
                StringWriter out = new StringWriter();
                JSONObject obj = new JSONObject(); 
                
                switch (username) {
                    case "RaphyBoy":
                        timestampMotion1 = theOutput;
                        obj.put("pictureData",picture1);
                        obj.put("message", "This is the data for the picture");
                        obj.writeJSONString(out);
                        pictureData1 = out.toString();

                        out = new StringWriter();
                        obj = new JSONObject(); 

                        //obj.put("timestampPicture", timestampPic);
                        obj.put("timestampMotion",timestampMotion1);
                        obj.put("message", "This is the time stamp for the motion");
                        obj.writeJSONString(out);
                        motionData1 = out.toString();
                        
                        publishMotion();
                        publishPicture();
                        break;
                    case "MateB":
                        timestampMotion2 = theOutput;
                        obj.put("pictureData",picture2);
                        obj.put("message", "This is the data for the picture");
                        obj.writeJSONString(out);
                        pictureData2 = out.toString();

                        out = new StringWriter();
                        obj = new JSONObject(); 

                        //obj.put("timestampPicture", timestampPic);
                        obj.put("timestampMotion",timestampMotion2);
                        obj.put("message", "This is the time stamp for the motion");
                        obj.writeJSONString(out);
                        motionData2 = out.toString();
                        
                        publishMotion();
                        publishPicture();
                        break;
                    case "Elidjay":
                        timestampMotion3 = theOutput;
                        obj.put("pictureData",picture3);
                        obj.put("message", "This is the data for the picture");
                        obj.writeJSONString(out);
                        pictureData3 = out.toString();

                        out = new StringWriter();
                        obj = new JSONObject(); 

                        //obj.put("timestampPicture", timestampPic);
                        obj.put("timestampMotion",timestampMotion3);
                        obj.put("message", "This is the time stamp for the motion");
                        obj.writeJSONString(out);
                        motionData3 = out.toString();
                        
                        publishMotion();
                        publishPicture();
                        break;
                }
                
                //obj.put("timestampPicture", timestampPic);

                setMyTiles();
                delay(10000);
                startSensorThread();
            }
            catch(IOException e){
                System.out.println("error");
            }
        });
        sensorThread.start();

### trigger activity of electronic component
 public void takePictureAndVideoThread(){
        Thread cameraThread = new Thread(() -> {
            var pi4j = Pi4J.newAutoContext();
            
            execute(pi4j);

            pi4j.shutdown(); 
        });
        cameraThread.start();
    }

## MQTT app
public class MyMqttMain {

    public static void main(String[] args) throws Exception {

        final String host = "1a12f4ea044641baa7e4680b0f4d7293.s2.eu.hivemq.cloud";
        final String username = "<your_username>";
        final String password = "<your_password>";

        // create an MQTT client
        final Mqtt5BlockingClient client = MqttClient.builder()
                .useMqttVersion5()
                .serverHost(host)
                .serverPort(8883)
                .sslWithDefaultConfig()
                .buildBlocking();

        // connect to HiveMQ Cloud with TLS and username/pw
        client.connectWith()
                .simpleAuth()
                .username(username)
                .password(UTF_8.encode(password))
                .applySimpleAuth()
                .send();

        System.out.println("Connected successfully");

        // subscribe to the topic "my/test/topic"
        client.subscribeWith()
                .topicFilter("my/test/topic")
                .send();

        // set a callback that is called when a message is received (using the async API style)
        client.toAsync().publishes(ALL, publish -> {
            System.out.println("Received message: " +
                publish.getTopic() + " -> " +
                UTF_8.decode(publish.getPayload().get()));

            // disconnect the client after a message was received
            client.disconnect();
        });

        // publish a message to the topic "my/test/topic"
        client.publishWith()
                .topic("my/test/topic")
                .payload(UTF_8.encode("Hello"))
                .send();
    }
}

## Compressed Code
{
    public class HelloWorld extends Application {
        public static void main(String[] args){
            launch(args);
        }

        @Override
        public void start(Stage stage){
            stage.setTitle("hello world");
            Button btn = new Button();
            btn.setText("Say 'Hello World' ");
            btn.setOnAction(new EventHandler<ActionEvent>(){
                @Override
                public void handle(ActionEvent event){
                    System.out.println("Hello World");
                }
            });

            StackPane root = new StackPane();
            root.getChildren().add(btn);
            stage.setScene(new Scene(root,300,250));
            stage.show();
        }
    }
    public class ProcessBuilder {
        private string theOuput;
        private ProcessBuilder processBuilder;
        public ProcessBuilder(String theApp){
            this.processBuilder = new processBuilder();

            boolean isWindows = System.getProperty("os.name").toLowerCase().startsWith("windows");

            List<String> commandAndArgs;
            if(isWindows){
                commandAndArgs = List.of("C:\\Dev\\Python3", theApp);
                this.processBuilder.command(commandAndArgs);
            }else{
                commandAndArgs = List.of("usr/bin/python3", theApp);
                this.processBuilder.command(commandAndArgs);
            }
        }
        String startProcess() throws IOException{
            this.theOutput = "";
            var process = this.processBuilder.start();
            try (var reader = new BufferedReader(
                new InputStreamReader(process.getInputStream()))) {
                    String line;
                    while ((line = reader.readLine()) != null){
                        this.theOutput = this.theOutput + line;
                    }
                }
            return this.theOutput;
        }
    }

    private void starExampleThread() {
        Thread exampleThread = new Thread(() -> {
            String theCMD = "";
            var theProcessBuilder = nwe ProcessBuilder(theCMD);
            try {
                String theOutput = theProcessBuilder.startProcess();
                takePictureNVideoThread();
            }
        })
    }
    private void takePictureNVideoThread(){
        Thread cameraThread = new Thread(() -> {
            var pi4j = Pi4j.newAutoContext();
            execute(pi4j);
            pi4j.shutdown();
        });
        cameraThread.start();
    }

    public static void main(String[] args) throws IOException {
        final String host = "";
        final String username = "mb";
        final String password = "";

        final Mqtt5BlockingClient client = MqttClient.builder()
        .useMqttVersion5()
        .serverHost(host)
        .serverPort(8883)
        .sslWithDefaultConfig()
        .buildBlocking();

        client.connectWith()
        .simpleAuth()
        .username(username)
        .password(UTF_8.encode(password))
        .applySimpleAuth()
        .send();

        client.subscribeWith()
        .topicFilter("my/test/topic")
        .send();

        client.toAsync().publishes(ALL , publish -> {
            System.out.println("Received message: " + publish.getTopic() + UTF_8.decode(publish.getPayload().get()));

            client.disconnect();
        });

        client.publishWith()
        .topic("my/test/topic")
        .payload(UTF_8.encode("Hello"))
        .send();

    }
}
