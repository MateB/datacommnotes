# DatacommNotes

Network : 2 or more devices linked together, can be physical or virtual link <br>
Every node in a network is connected

## HyperConnectivity
computer, smartphones, etc connect to digital networks <br>
Concept of being more connected to your home and office more than ever before
every device is more and more connected to each other and you can connect from anywhere to anything and place

## Advantages
few transactions require physical presence,
ability for remote transactions both personal and business,

## Drawbacks
frequently check phones,
expectation that everyone is always online,
difficult to detach from device, face2face convo is hard

## Devices that are Analog
telegraph, telephone, answering machine, fax machine

## Devices that are digital
computer, smartphone, internet, broadband/wireless

## Internet of things(IoT)
smart homes, cities, etc <br>
smart grid, industrial IoT<br>
smart farming, IoT tracking, etc<br>
- Hot topics:
    - data comm & security
    - IoT data analytics

## Evolution of communication
snail mail -> store and forward messages -> real time communication -> social media <br>
written docs -> email                   -> conversations/telepone/texting -> social networks

## Web impact on business
- E-commerce
    - doing business with remote customers over internet
    - schange how orgs carry out business transactions
- Web has changed how orgs market their goods

## IoT impact on business
- track everything
- data grows even faster
- administrive delays shrink
- waste will be easier to id
- identity and authorization management becomes more complex
- jobs or roles may change/go away

## Cloud impact on business
- cloud computing
    - renting another computer and network to run software to support your business
- Cloud service provider(CSP), private cloud and hybrid
    - CSP: business that offer cloud computing services
    - public cloud (public delivery model) Azure,AWS, GCP, etc
    - private cloud (private delivery model) an orgs data center or in a public cloud but accessible only by authorized users (++secure, ++$$$)
- Virtualization
    - VMs, dynamic provisioning, hypervisors, snapshot

- Everything as a service
    - IaaS
        - provions and launch of virtual machine
    - PaaS
        - develop applications
    - SaaS
        - rent access to specific software application
    - XaaS
        - extends SaaS solutions as professional services
        - security as a service, database as a service, etc

## Infratstructure as a Service (IaaS)
routers,swithces,servers, VM, load balancers, access points, storage, etc. Provided throug the onlione environement. Main clients: System admins

## Platform as a Service (PaaS)
servers, databases, OS, storage, development tools, etc. provided in an outside environment. Main clients: devs

## Software as a Service (SaaS)
Applications hosted by another company. Ex: ERP( entreprise resource planning) Office 365, GitHub, Google drive (any application such as learning, personal calendar, diet tracker) - available via web browsers. Main clients: end-users

## Anything as a Service(*aaS/AaaS/XaaS)
Desktop as a Service(DaaS), Virtual desktop, machine learning <br>
Function as a Service(FaaS), serverless computing <br>
Backend as a Service(BaaS), etc... <br>

## How are nodes connected in a network?
every node requires a network interface card(NIC) or some circuitry
the linking medium can be cables(twisted pair cooper, fiber-optic) or some wireless medium such as air

- telephone lines:
    - 2 wires
    - DSL (digital subscriber line)
    - 1 Gbps
- Ethernet:
    - 8 wires twisted
    - 100Mbps- 10Gbps
- Coaxial cable:
    - videotron
   -  2 Gbps
- Fiber optic cable:
    - 2-40Gbps
Wi-Fi:
    - coverage
    - range 20m indoors / 150m outdoors
    - 5.5-200Mbit/s
- Bluetooth:
    - range of 10m
    - 2.1Mbit/s
- Near field communication(NFC)
    - range 20cm
    - 424 kbit/s

## Network components and devices
- NIC
    - component that connects computer to network
- Transport mediums: cables or air
- Hubs
    - common medium inside the hub, if node 1 sends to node 2 its fine, if node 3 sends at same time this causes an issue
    - Hubs evolved into a switch, the difference is that the switch doesn't have a commond medium. In the switch there is a middle channel that establishes connection between different devices
- Bridge
    - device that has 2 NIC which allow you to connect to different networks. LAN 1 is connected to the bridge and LAN 2 also, allows both LAN's to communicate
    - between each of them. A bridge is a device that allows 2 different LAN's to communicate

- Switches
    - A switch can connect many devices and networks. Switches operate as an OSI layer 2 device. They use MAC addresses and build address tables of devices connected to each physical port. Switches that connect multiple devices or networks keep track of MAC addresses on all ports. When the switch sees an Ethernet frame destined for a specific MAC address it forwards that frame to that port
- Router
   - A router operates at netowrk layer. Same as a layer 3 switch, except its typically used for a wide area network circuit connections, campus backbone connections and building backbone connections. Routers can make intelligent decision on where to send packets. Instead of just reading the MAC address and forwarding table, routers can see the packet's Network Layer address or IP address. The network layer address contains info about the destination network number and host number. In essence, routers perform a path determination calculation to find the best path for the IP packets to traverse
- Access point
    - Device that creates a wireless local area network, or WLAN, usually in an office or large building. An access point connects to a wired router,switch or hub via calbe and projects WiFi signal to a designated area
- Cable modem
    - type of network bridge that provides bi-directional data communication via radio frquency channels on a hybrid fibre coaxial radio frequency over glass and coaxial cable infra.


## IP based communication
Topology: <br>
- point to point (2 computers only)
- star
- mesh <br>
Topology is different ways in which computers can be connected

Protocols: rules of communication <br>
- TCP/IP protocols (4 layers)
- OSI protocols ( 7 layers) <br>
OSI is a standard reference model. Meaning that the communication between each layer is made by API's

The idea of communication is sending data from one place to another. IP allows you to send data from one place to the specified destination using the IP. You can also send data on the same layer, if you send data on the physical layer of one device you can sed it to the other device on that layer using the OSI model.

Reference models are important as they dictate the way devices will communicate. It's a way to standardize the protocols.

## Circuit switching
Point A -> differnt circuits will be connected to reach the other end -> Point B (phone communications) <br>
If point C tries to connect to POint D while there is an ongoing call it must use different circuits than the ones in use. This cause a limit of number of calls
## Packet switching
Point A to Point B but in the middle there is a network. Data sent will be divided into multiple packets, each packet will be sent to the network, the network will proceed to take different paths for each packet until they all reach the final destination. Once arrived the packets will be assembled. <br>

In other words circuit has a limited number of communication that is possible each line has a specific circuit and can't reuse the same circuit. <br>
While packet switching uses a network that breaks down data into packets they each take different routes to reach their final destination, once arrived the packets are assembled.

## OSI & TCP/IP reference models
### Network protocol
Connected devices use protocols to communicate <Br>
Protocol: set of rules taht defines how two nodes communicate i.e.: how two nodes address each other and send data <br>
Multiple protocols taht are grouped together form a protocol suite or protocol family <br>
An implementation of the protocol suite is known as a protocol stack i.e.: software implementation that enables multiple protocols to work together

## OSI reference model
The OSI model breaks down the hundreds of questions to answer by a set of protocols:
- The questions are put into 7 groups called layers
    - Application (allow access to network resources)
    - Presentation (translate, encrypt and compress data)
    - Session (establish, manage and terminate sessions)
    - Transport (provide reliable process-to-process message delivery and error recovery)
    - Network (move packets from source to destination; to provide internetworking)
    - Data Link ( organize bits into frames; to provide hop-to-hop delivery)
    - Physical (transmits bits over a medium; to provide mechanical and electrical specificiations)

A protocol may answer only a few questions, in other words address specific layers in the model. By combinig multiple protocols into a protocol suite we can answer all the questions <br>
Layer # | OSI layer | Common protocols and applications | Common devices
--------| ----------| ----------------------------------| --------------
7       | Application | FTP, DHCP, HTTP | Gateway, firewall, endpoint devices(mobile,pc)
6       | Presentation| SSL, TLS        | Gateway, firewall, server
5       | Session     | NFS, L2F        | Gateawy, firewall, server
4       | Transport   | AH, TCP, UDP    | Gateway, firwall
3       | Network     | ICMP, IPv4, IPv6| router, brouter, layer 3 switch
2       | Data Link   | ARP, Ethernet(IEEE802.3)| Bridge, modem, network card, layer 2 switch
1       | Physical    | DSL, Wi-Fi, Bluetooth | Hub, repeater, cable, fibre, wireless

## TCP/IP model
TCP/IP is the primary protocol suite/stack that network nodes use to communicate with each other. It makes it possible for ocmputers to send and receive data via packet-switched networks. <br>
TCP: Transmission control protocol <br>
IP: internet protocol <br>
TCP/IP: protcol suite/stack which includes TCP, IP, and others

This model enables to transmit and receive data on the internet <br>
the 4 layers are:
- Application (consits of applications and processes taht use the network)
- Transport layer (provides end-to-end data delivery services)
- Internet layer (defines the datagram and handles the routing of data)
- Network Access Layer (consits of routines for accessing physical networks)
![image.png](./image.png)

The TCP/IP protocol suite was developed before OSI model. It doesnt not use the OSI model as a reference. <br>
Unlike OSI model the TCP/IP has 4 layers <br>
However the OSI reference model functions as a baseline for comparison to any protocol suite

## Network topology
The layout of how devices connect to a network, a map of the network that shows how devices connect to one another and how they use a connection medium. <br>
A node (server,computer, etc) can physically connect to a network and has an assigned IP hosts address
### physical topology
the picture of the actual network devices and medium that they use to connect to network
### logical topology
how the actual network workds and how data is transfered; view focuses more on how it operates

- BUS
    - all the nodes are connected to one main cable
    - the main cable acts as the backbone for the network
    - only one device communicates at a time
    - problems: collision (2 devices transmit same time) if main cable breaks, bandwidth(shared media)
    - bus topolgy is obsolete
- RING
    - All the nodes are connected in circular fashion
        - each node directly connected to the next
    - Token ring
        - attached devices need permission to transmit
        - permission is granted via token (small frame) that circulates around the ring
    - Obsolete
    - Problems: Bandwidth. If ring breaks (solution = dual ring)
- Hierarchical
    - each node can have an aritrary number of child nodes
    - parent nodes have more bandwidth
    - problem: if parent breaks , then children can't connect
- Mesh
    - each node connects to as many other nodes possible
    - messsages: send on a mesh network can take any possible paths from source to destination
    - problem: cost
    - solution: partial mesh
- Star/extended star
    - each node is connected (p2p) to a central connection point that can be a hub, switch or router
    - can be extended with repeaters
    - problem: if central connection point fails
## IP based Communications
TCP | IP
--- | ---
Guarantees deliver of reliable stream of data between two computer games | makes it possible to deliver packets across complex network destinations
one of the most popular protocols that orgs use to communicate | handles routing decisions necessary to get packets from source to destination
operates at OSI layer 4 | operates at OSI layer 3

- applications are designed and built knowing that TCP/IP is the transport protocol of choice
- standard is so prevalent that network presence is often expressed an IP address
    - if you have an IP address, you're on the network
- IP address identifies a device or computer to network as a unique node
- Private IP addresses identify nodes within an org
- Applications that want to communicate over networks only need to reference the IP address of the destination node

## Internetworking at OSI model
![image-1.png](./image-1.png)

- Internworking is a term used to describe connecting LAN's together
- LANs communicate using protocols
- Different network devices interoperate at different layers of the protocol stack

## Internetworking terms
### client/server architecture
Users interact with enterprise applications via local software running on a client device taht uses a network to connect to, and request services from, one or more servers <br>
Depends on client/server internetworking
### peer-to-peer internetworking
The use of an enterprise network for peers to exchange messages without depending on a central server to manage connections or message handling

Most of today's tech can support both ways of comms

### Intranet
An internal network that only employees can access <br>
a private network within that orgs IP network infrastructure
### Extranet
A remotely accessible network that an org makes accessible to its business and partners and suplpier through the public internet

## Data Encapsulation (TCP/IP)
When sending data it passes down the TCP/IP protocol stack. Each layer adds control information to ensure proper delivery. The control information is called a header, it gets placed in front of the data that is transmitted. Everytime data goes down one layer it gets added an additional header to the front of the stack. The addition of delivery information at every layer is called encapsulation.
![image-2.png](./image-2.png)

## Ethernet frame(Header + data + trailer)
- Header
    - preamble
    - SFD
    - Destination
    - Source
    - Type
- Data and pad
- Trailer
    - FCS

![image-4.png](./image-4.png)

## IPv4 address classes
![image-5.png](./image-5.png)

Class D addresses are used for multicasting (sending traffic to a group of devices like in a web conference session) <br>
Class E addresses are used for experimentation

![image-6.png](./image-6.png)

### example
IP address is 192.168.251.3, subnet mask is 255.255.255.0 and gateway is 192.168.251.1 <br>
What network are you on? 192.168.251.0 <br>
maximum number of computers? 254 because 2^8 is 256  but the 0 is for network address and 255 is broadcast and unable to use those. <br>
How does computer access internet? it uses the gateway address (192.168.251.1)

## Network Access Layer TCP/IP layer 1
Operates at the bottom layer of the TCP/IP reference model <br>
Reliability is the availability and integrity of data transmission <br>
Availability referes to uptime-whether there is a physical link access to the communication line <br>
Integrity refers to whether the data made it to the destination intact and accurate <br>
Transmission medium can be air, metal or glass

## Network Access Layer (Link + Physical)
TCP/IP reference model layer 1 is the network compare it to the OSI model it is both layers 1(physical) and 2 (network)

Where the bit sequence occurs (signaling of 1s n 0s), where transmission medai is needed to transit them and cabling, defines the frame format that the data is to follow

- IEEE 802.3
The standard for the physical and data link layer's media access control (MAC) of wired ethernet, the earlier versions defined CSMA/CD
- CSMA/CD
    - carrier-sense multiple acess with collision detection
        - CSMA/CD  is a protocol which helps avoid collision of data, how it works is it will listen to see if it can transmit, if it cannot it will wait for the data that is being transmitted to be transmitted and then will listen again to see if there's any other traffic, if there isnt any it will transmit.
    - No longer needed in full-duplex connections
    - Still supported for backward compatibility, half-duplex connections

## Media Access Control (MAC) address
used to send/receive packets, packets are received by network card, these packets include a header that contains the addressing info. The header contains an address called a hardware address, more commonly referred to as MAC address. <br>
Every network interface card (NIC) has a physical address that is burned into it when the card is manufactured. This address will not change for the life of the card.
 - 12 digit hexadecimal (48-bit) address
 - Ex: 98:90:96:c5:39:e4 (first 6 hex digits are manufacturere) (last 6 unique serial number assigned by the manufacturer)

## Data Encapsulation
A TCP/IP packet must contain the destinations MAC address <br>
As each packet arrives at the NIC the header is examined to see the target MAC address
    - if matches with MAC address of the NIC the packed is passed up the stack for processing
    - if no match then it's discarded
    ![image-9.png](./image-9.png)
    ![image-10.png](./image-10.png)

## Internetworking with Bridges
When internetworking at the data link layer (layer 2 OSI) it requires a bridge, it is the easiest way to interconnect 2 networks you can do this by placing a MAC layer bridge between the networks.
    - it examines the MAC address of each frame received
    - makes a filtering or forwarding decision based on MAC address forwarding table

## switch forwarding methods
![image-11.png](./image-11.png)

## Internetworking with switches
Switches connect multiple devices or networks keep track of MAC address on all ports, when switches see an Ethernet frame destined to a specific MAC address, it forwards that frame to that port. It uses MAC addresses to build a table of devices that is connected to each port.

### Layer 2 switch
Operates at the data link layer, examines the MAC layer addresses to the Ethernet frame

### Layer 3 switch
Operates at either the Data Link layer or Network Layer, they typically have a software that allows them to work like a layer 2 switch or multiport bridges. They usually operate at the network layer that examines the network layer address within the Ethernet frame, they can look up the destination IP address network number in their IP routing tables and then make a path determination decision.

#### summary on switches
Switches can connect many devices and networks. They operate as an OSI layer 2 device. They use MAC addresses and build a table of devices connected to each physical port. Switches keep track of MAC addresses on all ports. When the switch sees an ethernet frame destined for a specific MAC address it forwards that frame to that port.

Layer 2 switch operates at the Data Link layer and examines the MAC addresses of the Ethernet frames. Using this info, filtering and forwarding tasks are performed by the switch.

Layer 3 switch operates at the either Data Link or Network Layer. Layer 3 switches typically have software that allows them to function like layer 2 switch or multiport bridge. They usually operate at the Network Layer that examines the network layer address within the Ethernet frame.

The IP addresses typically are found in the Network Layer of the packet. An IP address is like a zip code for mailing a letter. It has both network number and host number, which is used to route IP packets to the appropriate destiantion IP network. A layer 3 switch can look up the destination IP network number in its IP routing table and then make a path determination decision. A layer 3 switch provides each device connected to its ports with its own Ethernet LAN segment. Layer 3 switches typically have resiliency or a redundant path connection, also called a redundant circuit, to a building or campus backbone network.

## Physical Layer
### Transmission media
- Copper
    - used in unshielded twisted pair (UTP, copper) and shielded twisted pair (STP, 8 wires) cable
    - UTP is the most common in office buildings and wired network environments
- Coaxial
    - Rugged indoor or outdoor cable
    - cable tv infrastructure is supported by coaxial cable to the residence
- Glass/Fiber
    - Fiber cables have a glass core surrounded by a glass cladding; light travels through glass core
    - Fiber optics can support large amounts of data transmissions over very far distances; often used for long-haul data transmissions

## Intro to signals
Signals are electric or electromagnetic impulses used to encode and transmit data, they can be analog or digital, computer networks and data/voice comms systems transmit signals
## Analog vs Digital
### analog
continuous wave form naturally occuring music and voice are examples, it is harder to separate noise from an analog signal than it from digital. <br>
Noise is unwanted electrical or electromagnetic energy that degrades the quality of signals
![image-13.png](./image-13.png)
### digital
is a discrete or non-continuous wave form, the signal can only appear in a fixed number of forms <br>
noise in a digital signal : can still discern a high voltage from low, if theres too much noise unable to discern high from low voltage

![image-12.png](./image-12.png)

## Fundamentals of Signals
### Amplitude
is the signal strength. the height of the wave above or below a given reference point. Amplitude is measured in volts.
![image-14.png](./image-14.png)

### Frequency
is the number of cycles per second.
- the number of times a signal makes a complete cycle within a given time frame.
    - measured in Hertz or cycles per second
- Spectrum
    - the range of frequencies that a signal spans from min to max
- Bandwidth
    - Absolute value of the difference between lowest and highest frequencies
![image-15.png](./image-15.png)

### Phase
position in time
- the position of the wave form relative to a given moment of time or relative time to zero
- a change in phase can be any number of angles between 0 and 360 degrees
- phase changes usually occur on common angles such as 45, 90, 135 etc...
![image-16.png](./image-16.png)

## Baud rate & bit rate
Serial communication is done at a predefined speed
- this speed is referred to as baud rate
- it refers to the number of bits transmitted per second
- the most frequently used baud rate is 9600bps
- lower baud rate or high baud rate can be used depending on the speed that can be handled reliable by the device
the sender and receiver need to handle the data at the same baud rate

## Internet and tranposrt layers
![image-17.png](./image-17.png)

### Internet Layer
Provides network addressing packets <br>
provides and supports routing technologies to direct packets to their destinations <br>
is the first layer that addresses issues with how to get packets from the source to the destination <br>
supports creating virtual circuits (predifined paths between two computers)

protocols that operate at the internet layer include, Internet protocol (IP), Internet Control Message Protocol (ICMP), Internet protocol security (IPSec)

### Transport Layer
Provides transparent and reliable data transfer between computers <br>
accepts data from upper layers and handles the details of getting the data to the destination computer <br>
services provided include:
- Flow control
- fragmentation/reassembly
- error control
- acknowledgement of delivery

Protocols that operate at transport layer include: TCP, UDP, Stream control transmission protocol (SCTP)

## Internet layer in detail
Contains the protocols that are resposnible for addressing and routing of packts. The internet protocol is responsible for detemining the source and destination IP address. A unique IP is assigned to every network node

Whereas the MAC address refers to the physical network card, the IP address refers to a logical address assigned to the node.
-Internet Protocol
    - Primary protocol that relays packets across most of today's diverse network
    - provides packet routing and host identification to deliver packets to their destinations
    - treats all packets seperately
    - Each packet contains a header with the destination IP address

### Internet protocol decentralization
IP networks are decentralized and dynamic <br>
Packet might not reach its destination node, or if it reaches its destination, might be different from orignal packet <br>
A network node can use a checksum to tell if a packet header has changed.

## IP addressing: IPv4 vs IPv6
Devices on IP-based networks, like the internet, need IP addresses <br>
Internet Engineering Task Force(IETF) develops and promotes internet standards, and publishes IPv4 and IPv6 <br>
Internet assigned numbers authority (IANA)
- is responsible for coordination IP addresses and resources around the world
IPv4 is sitll in primary use; slow transition to IPv6

## IPv4
- 32 bit addresses (4 bytes)
- usually displayed in dot notation
- four separate 8 bit numbers (octets)
- Octet value is between 0 and 255
- EX: 192.168.0.1
- IPv4 can be classful or classless networks

Dynamic Host Configuration Protocl (DHCP)
- A standard method for internal devices to request and receive IP addresses and configuration information

Network address translation (NAT) and private IP make it possible to continue using IPv4 in future.
- NAT is a method of mapping an IP address space into another by modifying network address information in the IP header of packets while they are in transit accross a traffic routing device

### IPv4 classful network architecture
Addressing architecture created five different type of networks based on required number of nodes
- IP address originally 5 classes: A B C D E
- A B C used for networks
- Each class restricted to IP address range
- Range based on number of nodes needed
- Max number is 2^32 addresses
![image-18.png](./image-18.png)

### IPv4 CIDR and Subnet Mask
CIDR (Classless Inter Domain Routing) | Subnet Mask
--------------------------------------| -----------
Replacement for classful network architecture | Is a binary number that contains all 1s in leftmost prefix length positions
Temporary solution for IP shortage address format | Subnet maks for the CIRD address block 168.12.0.0/16 is 11111111.11111111.00000000.00000000
Networks are split into groups of IP called CIDR blocks | Helps determine which network a given IP belongs to by indicating which part of an IP denotes the network and which is the host

### IPv4 private networks
Contains private IP addresses, not routable on the internet <br>
The purpose of private IP addresses is to allow orgs to assign their own IP addresses from private network ranges <br>
Network Address Translation (NAT) maps internal addresses to public routable addresses <br>
Port addresses translation (PAT) allows you to share a singel, public-facing IP address with a range of internal, private IP host addresses

## Resolving Addresses
Address resolution is process of finding an IP for a host name <br>
Domain name system (DNS) hierachical naming system that allows orgs to associate host names to IP addresses <br>
DNS
- servers store these associations and make the tables available for network users
- servers keep up with the changing host names and make it easy to react to any org taht changes its IP addresses
- is crucial to making the internet a usable network

## IPv6
Primary motivation for its creation was increase network address space, it uses 128-bit addresses (increases from 32 to 128). <br>
Maximum number of 2^218 addresses (>340 undecillion) <br>
Eight groups of 4 hex numbers, first 64 bits is network and last 64 is host (based on MAC)
![image-19.png](./image-19.png)

### IPv6 network methodologies
- Unicast : sending a packet to a single destination
- Anycast : sending a packet to the nearest node in a specified group of nodes
- Multicast : sending a packet to multiple destinations
- Broadcast : sends a packet to complete range of IP addresses

## IPv4 to IPv6
Dual IP stack operating systems support both IPv4 and IPv6 using two seperate network stacks for IP <br>
Literal IPv6 addresses
- colon character is a reserved character in network resource identifiers and Universal Naming Convention(UNC) path names
- For network resource identifiers, add square brackets around literal IPv6 addresses
- For UNC path names, convert colon characters to dashes and append ".ipv6-literal.net" domain to IPv6 literal addresses

## IP communications
When a packet enters its destination network, the network stack needs to know the packet's local destination. <br>
IPv4 uses ARP to provide local MAC address, while IPv6 uses the Neigbor Discovery protocol (NDP) <br>
ARP and NDP make it possible to use IP to transposrt traffic within local area networks and between networks, including those connected to the internet <br>
IP networks can exchange nearly any type of data between any two nodes on the network
- Type of supported traffic:
    - email messages
    - telephone calls
    - video conferencing
    - streaming audio and video
    - large file transfers
    - real-time data
    - instant messages

## Connectionless Versus Connection Oriented Communications
Most direct path to a destination may not be the best route if its congested <br>
IP is a connectionless protocol
- no notion of a connection between source and destination node
- treats each packet separately <br>
Aleternative is a connection oriented protocol
- sets up a connection between the source and destination
- treats each message separately <br>
Layered networking software enables a mix of connectionless and connection-oriented protocols. <br>
can use a connection-oriented protocol at the Transport Layer (layer 4) and still use IP for the Internet Layer (layer 3)
![image-20.png](./image-20.png)
![image-21.png](./image-21.png)
![image-22.png](./image-22.png)

## Internetworking with Routers
- Router
    - operates at network layer
    - is the same thing as a layer 3 switch but is typically used for WAN circuit connections, campus backbone connections and building backbone connections
    - Can make intelligent decisions on where to send packets
        - Instead of just reading the MAC address and forwarding a packet based on a forwarding table, it can see the packet's network layer address or IP address
        - network layer address contains infor about the destination network number and host number
        - performs a path determination calculation to find the best path for the IP packets to traverse
    - Typically has redundant processors and plenty of memory
    - Takes longer to examine a packet than for layer 2 switch or bridge

In other words: a router operates at Network Layer. Same as a layer 3 switch, except it's typically used for a wide area network circuit connections. Routers can make intelligent decisions on where to send packets. Instead of just readint the MAC address and forwarding table, it can see the packet's Network Layer address or IP address. The network layer address contains info about the destination network number and host number. Basically routers perform a path determination calculation to find the best path for the IP packets to traverse.

## Internetworking with Gateway
Interconnects two networks that use different protocols, translates network packets from one network protocol to another. <br>
Main job is to translate all packets to protocols compatible with destination network. Is commonly placed at entry and exit points of networks.<br>
Commonly rungs either as software on comp or as a device that performs same functions as router.

### Routing concepts
- Network Layer Address (IP)
- Static route
- Dynamic route

## Resiliency and Redundancy
Col 1 | Col 2
------|------
Layer 2 resiliency | enabling resilience protocols, such as spanning tree protocol (STP) and resilient ethernet protocol (ERP) to avoid network comms interruptions
Layer 3 resiliency | supporting alternate routes to a WAN when the primary route is unavailable, ex: Cisco Hot Standby Routing Protocol (HSRP)
Virtual networking components | Implementing some network devices as virtual machine (VMs) or containers, or building virtualized networks
Network Storage Types | Implementing network as storage devices as VMs; storage area networks and network attached storage

## Transport Layer
- Determines whether the sender and the receiver will set up a connection before communication
- The transport layer has two protocols:
    - Transmission Control Protocol (TCP)
        - TCP sets up a connection and sends acknowledgements (ACK)
        - TCP guarantees safe delivery
        - TCP provides connection oriented reliable comms
    - User Datagram Protocol (UDP)
        - UDP doesn't set up a connection and doesn't use ACK
        - UDP can move data faster but delivery isnt guaranteed
        - UDP provides connectionless, unreliable comm
- A conversation is started with a three-way handshake to set up a TCP connection which is used for the duration of the communication session

## Port
- Processes requests for data or services
- Ports and sockets are used to receive and send data
    - PORT: as a packet is moving up through the protocol stack on its way to the application layer, the Transport layer direccts the packet to the appropriate port
        - a port is a number that the application at the Application Layer uses as a send-n-receive address
        - applications listen for request at the ports to which they are assigned
        - TCP and UDP have 65,536 ports
## Socket
- It processes request for data or services
- Ports n sockets used to receive and send data
    - SOCKET: endpoint of a specific connection
    - Combines three pieces of info, IP, TCP or UDP and port number
        - TCP/UDP indicates which protocol is to be used
        - a socket is written like this:
            - 198.168.48.36:TCP:80 or 198.168.48.36:UDP:53

## Raspberry pi 4B components
- 40 pin General purpose input/ouput (GPIO) header
    - gpio is the interface for connecting components such as LED, resistors, breadboards etc
    - each of the 40 pins has a specific function, most can be used as either input or output depending on your program or its function
    - 3 ways to number the pins
        - GPIO numbering : pin numbers assigned by Broadcom, the processing chip manufacturer
        - Physical numbering : counting accross and down from pin 1
        - WiringPI GPIO numbering : pin number reference used by wiring Pi code
- Serial Peripheral Interface (SPI)
    - Synchronuous serial bus commonly used for sending data between micro controllers and small external peripherical devices
    - Multiple peripherical devices can be interface as SPI slaves and be controlled with the SPI master
    - SPI uses separate clock (SCK) and data lines (MISO, MOSI) along with select line (SS) to choose among slave devices
    - RPI supoprts 2 chip select (CE) lines to interface with 2 SPI slave devices : GPIO 7,8
- Inter-Intergrated Circtuit (i2C)
    - i2c interface allows multiple slave integrated circuits to communicate with one or more master
    - i2c uses two directional lines: serial data line (SDA) and serial clock line (SCL)

## Accessories
### Breadboard
- device that allows one to connect circuit without soldering
- power rails of a breadboard are used for connecting power source to the board
### GPIO extension board
- provides convenient way to connect GPIO ports to the breadboard
- GPIO pin sequence is identical to GPIO pin sequence on RPi
### LED
- type of diode
- diode only works if the current flows in the correct direction
- LED lights up if longer is connected to positive output of power source and shorter pin to ground
- Diodes only work if voltage of positive electrode is higher than negative
- NOTE: resistors with specified resistance must be connected in series to the LED you plan to use
- Recall that V=IR
### Resistor
- passive electrical components that limit/regulates flow of current
- measured in Ohms
- Bands of color on resistor is used to identify its value
- unlike LEDs n diodes, resistors are non polar
### ADC module
- analog to digital converter is an integrated circuit used to convert analog signals to digital form
### Potentiometer
- resistive element with 3 terminal parts
- unlike resistors which have fixed resistance, the resistance value of it can be adjusted
- often consists of resistive substance ( wire or carbon element) and a movable contact brush
### Rotary potentiometer
- rotary and  linear potentiometer have similar function
- only difference is physical action, rotation or sliding
### Thermistor
- temperature sensitive resistor
- senses change in temp, resistance of its changes
- this property can be used to detect temp intensity
 
## Security Goals
- Confidentiality: Protect sensitive data
    - Prevent unauthorized entities from accessing the data
- Integrity: Ensure no authorized modifications
    - Protect data from unauthorized change
- Availability: Authorized entity can access it
    - Data be accessible to an authorized entity when it is needed
- Examples
    - Implement personal system and internet security
    - Vulnerability/security management
        - Vulnerability: security weakness in a system
    - Prevent security attacks (threats, malware, data leaks, social engineering, phishin, fake networks, etc)

## Encryption
Encryption is the process of converting data into unreadable format by scrambling its content so it can only be read by someone who the correct encryption key to unscramble it.
 - plain text: message not encrypted
 - cipher text: encrypted message
 - two method:
    - Symmetric key encryption
    - Asymmetric key encryption

## Symmetric

### Symmetric key encryption
    - encryption and decrypt uses same key
    - problem : exchange the key securely
    - Faster

### Symmetric key algorithms
Algorithms in which the same key is used for both encrypt and decrypt
![image-23.png](./image-23.png)

### Types of symmetric-key algorithms
- block ciphers
    - encrypt data one block at a time
        - typical block size 64 or 128 bits
- Stream ciphers
    - encrypt data one bit or one byte at a time

## Symmetric encryption > Block ciphers
Data Encryption Standard (DES)
    - uses a 56 bit key to encode 64 bit blocks of data
    - insecure due to small key size
        - key can be ascertain using exhaustive search within few minutes
TripleDES
    - Uses DES 3 times in tandem
        - output of 1 DES input to the next DES
    - Is also insecure and should not be used
Advanced Encryption Standard (AES)
    - Replacement of DES
    - 3 possible key sizes, 128, 192 or 256 bits keys
    - Uses 128 bit block size

## Symmetric encrpytion > Stream ciphers
Typically encrypt/decrypt data faster than block cipher
 - RC4
    - most widely known stream cipher
    - used in WEP (Wired Equivalent Privacy) and WPA (Wi-Fi Protected Acess) and TLS/SSL
    - Insecure, should no longer be used

## Asymmetric

### Asymmetric key encryption
    - 2 keys are invovled, public to encrypt and private to decrypt the message
    - Keys are manageable and no need to securely exchange the public key
    - slower

### Asymmetric key general idea
also referred to as public-key encryption algorithms
![image-24.png](./image-24.png)

### Asymmetric key algorithms
RSA is the most common used algorithm
 - based on difficulty of factoring prime numbers
 - supports varaible length key sizes ( 512, 0124, 2048, 3072)
 - current recommended key size : min 2048 bits

### Encryption algorithms key strength
Strength of cryptographic algorithms is determined by the length of the key in bits <br>
set of possible keys for a crypthographic algorith is called key space
    - 128 bit key there are 2^128 possible keys
    - to crack the key one must use brute-force

## Java implementation
### JAVA AES Encrypt and Decrypt
6 modes of operation for symmetric key crypto schemes; each mode divides plaintext into 128-bit blocks:
    - ECB (Electronic Code Book): Each block of the plaintext is encrypted with the same key; thus, it produces the same result for the same block
        - not recommended for encryption
    - CBC (Cipher Block Chaining): the first block of plaintext is XOR with Initialization Vector (IV) the block of ciphertext is then XORed with the next block of plaintext
    - CFB (Cipher FeedBack): First the IV is encrypted the result is XORed to the first block of plain text the result of the last operation is encrypted and the next result XORed to the next block
        - this mode can be used as a stream cipher
    - OFB (Output FeedBack): Similar to CFB except that it's the new IV that is encrpyted each time to be XORed with blocks of plain text
        - can also be used as stream cipher
    - CTR (Counter MOde): this mode uses a counter as an IV; similar to OFB except that the counter gets encrypted each turn and the result is XORed with blocks of plaintext
    - GCM (Galois/Counter Mode): this mode is an extension of CTR, in addition to encrypting it also calculates an authentication  tag that is appended to the ciphertext (128 bits long)
        - recommended by NIST (National institute of standard and technology)

### Implementing AES-GCM in java
    - AES-GCM is most widely used authenticated cipher
    - AES-GCM inputs:
        - input data
        - AES secret key (128, 192, 256 bits)
        - 12 bytes (96-bit) IV
        - lengths in bits of authenticated tag (128 bits)
    Note 12 bytes IV is prefixed to ciphertext because the save IV is needed for decrypt
## Integrity
### Hash Function
A cryptographic hash function is an algorithm that takes data or message of any length and produces a fix-length output called message digest or fingerprint <br>
if a hash function is to be useful it must be strongly collision-free
    - a collision occurs for a hash function when 2 different messages give the same message digest
- Objective : data integrity
Examples of hash functions:
    - MD5
        - produces 128 bit digest
        - collision against MD5 can be calculated within seconds; therefore this hash function is insecure
    - SHA-1 (Secure Hash Algorithm 1)
        - produces 160-bit digest
        - collision against SHA-1 can be calculated within a relative short time period; therefore it is insecure
    - RIPEMD-160 (race integrity primitives evaluation message digest)
        - produces 160 bit digest
    - SHA-2 
        - 2 hash algorithms classified as SHA-2; SHA-256 and SHA-512
    - SHA-3
        - provides the same output sizes as SHA-2
### Message authenticaiton Code (MAC)
similar to message digest produced by hash functions, except that a secret key is required to produce the MAC <br>
- HMAC (hash-based MAC) is the algorithm thats used along with secret key to produce message authentication code
    - any cryptographic hash function can be used to construct HMAC
    - examples: HMAC-SHA-256, HMAC-RIPEMD-160
-Objective: data integrity and authenticity

#### password hashing
- regular hash functions are not suitable for password hashing heres why
    - the availability of faster computers make it easier for dictionary attack
    - processor speed and CPU cores will continue to increase
- a solution for this problem is to use hashing algorithms whose speed can be slowed down
    - if the time it takes to produce hash password increases the probability of attackers discovering password decreases (brute force attack)
- password hashing fucntions thus need to have configurable hash-time parameter
    - Argon2 is currently the recommended choice of hash functions that satisfied this requirement
#### Argon 2 algorithm
- Argon2 accepts 5 parameters
    - salt length: length of random salt, rec 16 bytes
    - hash length: length of gen. hash, rec 16 or 32 bytes
    - iterations: number of iterations, affect time cost
    - Memory: amount of memory used by algorithm (in kibibytes, 1k = 1024 bytes) affect memory cost
    - Parallelism: Number of threads ( or lanes ) used by the algorithm, affect the degree of parallelism
- Argon 2 algorithm has 3 variants
    - Argon2d: maximizes resistance to GPU cracking attacks suitable for crypto
    - Argon2i: optimizes to resist side channel attacks suitable for password hashing
    - Argon2id: hybrid version

### Digital signature
- uses a pair of private-public keys and consists of signing algorithm and verification algorithm
- the signing alogrithm uses a private key to generate signature
- verification algorithm uses signers public  key to decrypt signature
- to reduce the size of digital signatures, message is first hashed using hash function then message digest is signed
#### Digital signature schemes
 - RSA
 - DSA (Digital Signature Algorithm)
 - recommended key length for both is at least 2048 bits
 - Elliptic Cure Digital Signature Algorithm (ECDSA)
 - EdDSA (Edwards-Curve Digital Signature Algorithm)
 ![image-25.png](./image-25.png)

### Digital certificate
digital certificate( or pub key certificate) electronic document that bings the value of pub key, sender info, and length of time the certificate is to be considered valid. <br>
Issued by a certificate authority (CA)
    - a trusted entity that creates, signs and manages certificates
    - a CA is required to sign a digital certificate to attest its validity
### User and server authentication
Commonly used authentication schemes
    - passwords
    - digital certificates
    - radio frequency indentifcations (RFID) cards
    - biometrics
Many applications use certificates
    - examples https protocol
        - web browsers validate an HTTPS web site using digital ceritficate
        - web server sends a session key
        - secure interaction between client and server

## Transport layer security Intro
TLS was designed to operate on top of a reliable transport protocol such as TCP
    - however there is also the datagram transport layer security (DTLS) protocol which is based on TLS
- TLS protocol is designed to provide three essential services to all applications running above itL
    - encryption
    - authentication
    - data integrity
### Overview
in order to establish a cryptographically secure data channel, the connection peers must agree on the following
    - cipher suites that will be used and the keys used to encrypt the data
- the  <b> TLS handshake protocol <b> is responsible for performing this exchange
- during TLS handshake phase the communicating peers authenticate their identity
- when used in the browser this authentication mechanism allows the client to verify that the server is who it claims to be
    - this verification is based on the established chain of trust
- the server can also optionally verify the identity of the client
- the communcation peers use public key cryptography to negotiate a share secret key
    - this is done wihtout having to establish prior knowledge of each other
    - the negotiation is done over a secure channel
### TLS Handshake
before the client and the server can begin exchaning data over TLS the encrypted tunnel must be negotiated <br>
the communicating peers must agree on the following:
    - version of TLS protocol to use
    - the cipher suite that will be used to provide confidentiatlity, integrity and authentication
    - server certificate and optionally the client certificate must also be verified
### Diffie-Hellman key exchange
Diffie-Hellman key exchange allows client and server to negotiate a shared secret without explicitly communicating it in handshake
    - servers private key is ussed to sign and verify handshake
    - however established symmetric key never leaves the client or server and cannot be intercepted by a passive attacker even if they have access to private key
### TLS Session Resumption
the extra latency and computational costs of the full TLS handshake impose a serious performance penalty on all applications that require secure communication
    - to help mitigate some of the costs, TLS provides a mechanism to resume or share the same negotiated secret key data between multiple connections
#### 
